#-------------------------------------------------
#
# Project created by QtCreator 2015-03-11T18:34:52
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = ECOM008_101
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    pessoa.cpp \
    politico.cpp

HEADERS += \
    pessoa.h \
    politico.h
