#ifndef POLITICO_H
#define POLITICO_H

#include "pessoa.h"

class Politico : public Pessoa
{
    double m_salario;
public:
    Politico(QString nome, QString cpf, quint8 idade, double salario);
    ~Politico();
    void setNome(const QString &nome);
};

#endif // POLITICO_H
