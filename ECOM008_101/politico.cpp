#include "politico.h"
#include <QDebug>

Politico::Politico(QString nome, QString cpf, quint8 idade, double salario):
    Pessoa(nome, cpf, idade)
{
    m_salario = salario*m_idade;
}

Politico::~Politico()
{

}

void Politico::setNome(const QString &nome)
{
    qDebug() << Q_FUNC_INFO;
    m_nome = nome;
}

