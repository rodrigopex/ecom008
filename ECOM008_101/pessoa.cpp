
#include "pessoa.h"
#include <QDebug>


QString Pessoa::nome() const
{
    return m_nome;
}

void Pessoa::setNome(const QString &nome)
{
    qDebug() << Q_FUNC_INFO;
    m_nome = nome;
}

QString Pessoa::cpf() const
{
    return m_cpf;
}

void Pessoa::setCpf(const QString &cpf)
{
    m_cpf = cpf;
}

quint8 Pessoa::idade() const
{
    return m_idade;
}

void Pessoa::setIdade(const quint8 &idade)
{
    m_idade = idade;
}

Pessoa *Pessoa::filho() const
{
    return m_filho;
}

//Pessoa::Pessoa() {
//    m_filho = 0;
//    m_nome = "";
//    m_cpf = "";
//    m_idade = 18;
//    qDebug() << "Const";
//}

Pessoa::Pessoa(QString nome, QString cpf, quint8 idade) {
    m_filho = 0;
    m_nome = nome;
    m_cpf = cpf;
    m_idade = idade;
    qDebug() << "Const" << m_nome << m_cpf << m_idade;
}

Pessoa::~Pessoa() {
    qDebug() << "Dest" << m_nome << m_cpf << m_idade;
    if(m_filho != 0) {
        delete m_filho;
    }
}

void Pessoa::addChild(Pessoa *p)
{
    this->removeChild();
    if(p)
        m_filho = p;
}

void Pessoa::addChild(const QString &nome, const QString &cpf, quint8 idade)
{
    this->addChild(new Pessoa(nome, cpf, idade));
}

void Pessoa::removeChild()
{
    if(m_filho != 0) {
        delete m_filho;
        m_filho = 0;
    }
}

void Pessoa::inc() {
    this->a++;
}

void Pessoa::inc(char) {
    qDebug() << "inc B";
}

void Pessoa::inc(int b) {
    Q_UNUSED(b)
    qDebug() << "inc A";
}
