#ifndef PESSOA_H
#define PESSOA_H

#include <QString>

class Pessoa {
    QString m_cpf;
    Pessoa * m_filho;
protected:
    QString m_nome;
    quint8 m_idade;
public:
    int a;
//    Pessoa();
    Pessoa(QString nome, QString cpf, quint8 idade);
    ~Pessoa();
    void addChild(Pessoa *p);
    void addChild(const QString &nome, const QString &cpf, quint8 idade);
    void removeChild();
    void inc();
    void inc(char c);
    void inc(int b);
    QString nome() const;
    virtual void setNome(const QString &nome);
    QString cpf() const;
    void setCpf(const QString &cpf);
    quint8 idade() const;
    void setIdade(const quint8 &idade);
    Pessoa *filho() const;
    // virtual void metodoObrigatorioNoFilho() = 0; Método virtual puro
};

#endif // PESSOA_H
