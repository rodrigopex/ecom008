

#include <QDebug>
#include <QString>
#include "pessoa.h"
#include "politico.h"

struct pessoa {
    int a;    
};

void inc(struct pessoa * p) {
    p->a++;
}

void inc(struct pessoa * p, int a) {
    p->a++;
}


int main(int argc, char *argv[])
{
    //C
    struct pessoa p;
    p.a = 1;
    inc(&p);
    qDebug() << "Pessoa estrutura:" << p.a;

    //C++

    Pessoa p1("Hello", "055", 30);
    p1.a = 2;
    p1.inc();

    qDebug() << "Pessoa classe:" << p1.a;

    Pessoa *p2 = new Pessoa("Fulano", "055.345.142-96", 57);
    p2->addChild("Fulano 1", "000.345.765-29", 13);
    p2->filho()->addChild("Cicrano", "111.222.333-44", 0);
    p2->filho()->filho()->addChild("Cicrano 2", "111.222.333-44", 0);
    p2->addChild("Fulano 2", "000.345.765-29", 13);
    p2->addChild("Fulano 3", "000.345.765-29", 13);
    p2->removeChild();
    qDebug() << "Pessoa classe:" << p2->a;

    Politico *pol = new Politico("Deputado", "000.000.000-11", 50, 46000.0);
    Pessoa *pTest = pol;
    pTest->setNome("Foi");
    qDebug() << "Novo nome:" << pTest->nome();
    pol->addChild("Deputado futuro", "111.222.333.222-11", 20);
    pol->addChild(new Politico("Futuro deputado", "111.222.333.222-11", 8, 5000));
    delete pol;

    return 0;
}
















